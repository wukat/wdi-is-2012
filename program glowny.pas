program macierze;
uses crt, matrx;

var 
  n, m : integer;
  a, b, c : matrix;

begin
n := 3;
m := 3;


clrscr;
a := fill_matrix(n, n);
b := fill_matrix(n, n);
c := multiply_matrices(a, b, n, n, n);
show_matrix(c,n,n);
repeat until keypressed;
end.
