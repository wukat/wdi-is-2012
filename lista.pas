program lista_dwukierunkowa;
uses crt;

type list_pointer = ^list;
  list = record
    nastepny,poprzedni : list_pointer;
    zawartosc : integer
  end;

var poczatek,koniec, pomocny : list_pointer;
  wartosc : integer;
  decision : char;


procedure wstawianie(var aktualny : list_pointer; wartosc : integer);
var nowy : list_pointer;
begin
  new(nowy);
  nowy^.zawartosc := wartosc;
  if (poczatek = NIL) then 
  begin
    nowy^.nastepny := NIL;
    nowy^.poprzedni := NIL;
    poczatek := nowy; 
    koniec := nowy;
  end
  else 
  begin
    if (aktualny^.nastepny = NIL) then
    begin
      koniec := nowy;
      nowy^.nastepny := NIL;
    end
    else
    begin
      aktualny^.nastepny^.poprzedni := nowy;
      nowy^.nastepny := aktualny^.nastepny;
    end;
    aktualny^.nastepny := nowy;
    nowy^.poprzedni := aktualny;
  end;
end;


procedure usuwanie(aktualny : list_pointer);
begin
  if (aktualny^.nastepny = NIL) then
    koniec := aktualny^.poprzedni
  else
    aktualny^.nastepny^.poprzedni := aktualny^.poprzedni;
  if (aktualny^.poprzedni = NIL) then
    poczatek := aktualny^.nastepny
  else
    aktualny^.poprzedni^.nastepny := aktualny^.nastepny;
  dispose(aktualny);
end;


procedure wyswietl();
var wyswietlany: list_pointer;
begin
  wyswietlany := poczatek;
  while (wyswietlany <> NIL) do
  begin
    writeln(wyswietlany^.zawartosc);
    wyswietlany := wyswietlany^.nastepny;
  end;
  repeat until keypressed;
end;


function seek(wartosc : integer) : list_pointer;
var poszukiwany : list_pointer;
begin
  seek := NIL;
  poszukiwany := poczatek;
  while (poszukiwany <> NIL) do
  begin
    if (poszukiwany^.zawartosc = wartosc) then
      seek := poszukiwany;
    poszukiwany := poszukiwany^.nastepny;
  end;
end;


begin
  poczatek := NIL;
  koniec := NIL;
  pomocny := NIL;
  repeat
    clrscr;
    writeln('Podaj szukana wartosc');
    readln(wartosc);
    if seek(wartosc) = NIL then 
    begin
      pomocny := poczatek;
      writeln('Nie znaleziono wartosci.');
      repeat 
         writeln('1. Dodaj element do listy (jako pierwszy).'); 
         writeln('3. Wysietl liste.');
         writeln('4. Zakoncz.');
         decision := readkey;
      until (decision = '1') or (decision = '3') or (decision = '4');
    end
    else
    begin
      pomocny := seek(wartosc);
      repeat
        writeln('1. Dodaj element do listy (za znalezionym).');
        writeln('2. Usun znaleziony element listy.');
        writeln('3. Wysietl liste.');
        writeln('4. Zakoncz.');
        decision := readkey;
     until (decision = '1') or (decision = '2') or (decision = '3') or (decision = '4');
    end;
    case decision of
      '1': wstawianie(pomocny, wartosc);
      '2': usuwanie(pomocny);
      '3': wyswietl();
    end;
  until (decision = '4');
end.	 	