program konwersja;
uses crt;

var 
  base, base2 : byte;
  number, result : string;
  decimal_number : extended;


function input_base(which : string) : byte;
var
  system : string;
  result : byte;
  error : byte;

begin
  repeat
    writeln('Podaj podstawe systemu ', which, ' (od 2 do 16):');
    readln(system);
    val(system, result, error);
    if result > 16 then error := 3;
    if result < 2 then error := 3;
    if (error <> 0) then writeln('Blad! Sprobuj ponownie!');
  until error = 0;    
  
  input_base := result;
end;


function check_number(number : string; base : byte) : word;
var
  i : byte;
  report : string;

begin
  report := ' ';
  for i := 0 to length(number) do
    begin
      if ((ord(number[i]) - ord('0') <= 9) and (ord(number[i]) - ord('0') < base)) 
        then begin if (ord(number[i]) - ord('0') >= 0) then report := report end
        else
          if (((ord(number[i]) - ord('a') + 10) >= 10) and ((ord(number[i]) - ord('a') + 10) < base)) and (((ord(number[i]) - ord('a') + 10) <= 16))
          then report := report
          else report := report + '1';
    end;
  if report = ' ' then check_number := 1 else check_number := 0;
end; 


function input_number(base1 : byte) : string;
var
  number_int, number_frac : string;
  error : byte;

begin
  error := 0;
  repeat
    writeln('Podaj liczbe w systemie ', base, ' (od 2 do 16).');
    writeln('Cyfry wieksze niz 9 zastap odpowiednimi malymi literami.');
    write('Podaj czesc calkowita: ');
    readln(number_int);
    if (number_int = '') then number_int := '0';
    write('Podaj czesc ulamkowa (np. 001): ');
    readln(number_frac);
    if (number_frac = '') then number_frac := '0';
    if ((check_number(number_int, base1) = 0) or (check_number(number_frac, base1) = 0))
      then begin writeln('Blad! Zla liczba!'); error := 1; end 
      else error := 0;
    
  until error = 0;

  input_number := number_int + '.' + number_frac;
end;


function change_int_into_decimal(number : string; base : byte) : longword;
var
  result_int: longword;
  i : byte;

begin
  result_int := 0;
  
  for i := 1 to length(number) do 
  begin
    if ((ord(number[i]) - ord('0') <= 9) and (ord(number[i]) - ord('0') < base))
    then result_int := (result_int + ord(number[i]) - ord('0')) * base
    else if ((ord(number[i]) - ord('a') + 10) >= 10) and ((ord(number[i]) - ord('a') + 10) < base)
         then result_int := (result_int + ord(number[i]) - ord('a') + 10) * base;
  end;
  result_int := result_int div base;

  change_int_into_decimal := result_int;
end;


function change_real_into_decimal(number : string; base : byte) : extended;
var
  number_int, number_frac, divisor : string;
  i, length_of_int_part : byte;
  result_int : longword;
  result_frac : extended;

begin
  number_int := '';
  number_frac := '';
  for i := 1 to length(number) do
    if (number[i] = '.') then length_of_int_part := i - 1;
  for i := 1 to length_of_int_part do
    number_int := number_int + number[i];
  result_int := change_int_into_decimal(number_int, base);
  
  divisor := '1';
  for i := length_of_int_part + 2 to length(number) do
    begin
      number_frac := number_frac + number[i];
      divisor := divisor + '0';
    end;
  result_frac := change_int_into_decimal(number_frac, base) / change_int_into_decimal(divisor, base);
  
  change_real_into_decimal := result_int + result_frac;
end;


function change_int_into_second_base_number(number : longword; base : byte) : string;
var
  result : string;

begin
  result := '';
  repeat
    if ((number mod base) <= 9) 
    then result := result + char(ord('0') + (number mod base))
    else result := result + char(ord('a') + (number mod base) - 10);
    number := number div base;
  until number = 0;
 
  change_int_into_second_base_number := result;
end;


function change_real_into_second_base_number(number : extended; base : byte) : string;
var
  result, result1 : string;
  i : byte;
  number_frac : extended;

begin
  result := '';
  result1 := change_int_into_second_base_number(trunc(number), base);
  for i := length(result1) downto 1 do
    result := result + result1[i];
  result := result + '.';
  
  number_frac := number - trunc(number);
  i := 1;
  repeat
    if (trunc(number_frac * base) < 9) 
    then result := result + char(ord('0') + trunc(number_frac * base))
    else result := result + char(ord('a') + trunc(number_frac * base) - 10);
    number_frac := (number_frac * base) - trunc(number_frac * base);
    i := i + 1; 
  until (number_frac = 0) or (i = 9);
      
  change_real_into_second_base_number := result;
end;


procedure print_all(base : byte; base2 : byte; number : string; number2 : string);
var
  i : byte;
begin
  clrscr;
  writeln('Liczba ', number, '[', base, '] to w systemie ', base2, '-owym ', number2, '.');
end;


begin
  clrscr;
  base := input_base('pierwszego');
  number := input_number(base);
  decimal_number := change_real_into_decimal(number, base);
  base2 := input_base('drugiego');
  result := change_real_into_second_base_number(decimal_number, base2);
  print_all(base, base2, number, result);
  
  writeln('Nacisnij dowolny klawisz, aby zakonczyc.');
  repeat until keypressed;
end.