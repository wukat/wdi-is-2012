program konwersja;
uses crt;

var 
  base, base2 : byte;
  number, reverse_result : string;
  decimal_number : longword;


function input_base(which : string) : byte;
var
  system : string;
  result : byte;
  error : byte;

begin
  repeat
    writeln('Podaj podstawe systemu ', which, ' (od 2 do 16):');
    readln(system);
    val(system, result, error);
    if result > 16 then error := 3;
    if result < 2 then error := 3;
    if (error <> 0) then writeln('Blad! Sprobuj ponownie!');
  until error = 0;    
  
  input_base := result;
end;


function check_number(number : string; base : byte) : word;
var
  i : byte;
  report : string;

begin
  report := ' ';
  for i := 0 to length(number) do
    begin
      if ((ord(number[i]) - ord('0') <= 9) and (ord(number[i]) - ord('0') < base))
        then report := report
        else
          if ((ord(number[i]) - ord('a') + 10) >= 10) and ((ord(number[i]) - ord('a') + 10) < base)
          then report := report
          else report := report + '1';
    end;
  if report = ' ' then check_number := 1 else check_number := 0;
end; 


function input_number(base1 : byte) : string;
var
  number : string;
  error : byte;

begin
  error := 0;
  repeat
    writeln('Podaj liczbe w systemie ', base, '.');
    writeln('Cyfry wieksze niz 9 zastap odpowiednimi malymi literami.');
    write('Twoja liczba: ');
    readln(number);
    if (check_number(number, base1) = 0) 
      then begin writeln('Blad! Zla liczba!'); error := 1; end 
      else error := 0;
  until error = 0;

  input_number := number;
end;


function change_into_decimal(number : string; base : byte) : longword;
var
  result : longword;
  i : byte;

begin
  result := 0;
  for i := 1 to length(number) do
    begin
      if ((ord(number[i]) - ord('0') < 9) and (ord(number[i]) - ord('0') < base))
        then result := (result + ord(number[i]) - ord('0')) * base
        else
          if ((ord(number[i]) - ord('a') + 10) >= 10) and ((ord(number[i]) - ord('a') + 10) < base)
          then result := (result + ord(number[i]) - ord('a') + 10) * base;
    end;
  result := result div base;

  change_into_decimal := result;
end;



function change_into_second_base_number(number : longword; base : byte) : string;
var
  result : string;
begin
  result := '';
  repeat
    if ((number mod base) < 9) 
    then result := result + char(ord('0') + (number mod base))
    else result := result + char(ord('a') + (number mod base) - 10);
    number := number div base;
  until number = 0;
 
  change_into_second_base_number := result;
end;


procedure print_all(base : byte; base2 : byte; number : string; number2 : string);
var
  i : byte;
begin
  clrscr;
  write('Liczba ', number, '(', base, ') to w systemie ', base2, '-owym ');
  for i := length(number2) downto 1 do
    write(number2[i]);
  writeln('.');
end;


begin
  clrscr;
  base := input_base('pierwszego');
  number := input_number(base);
  decimal_number := change_into_decimal(number, base);
  base2 := input_base('drugiego');
  reverse_result := change_into_second_base_number(decimal_number, base2);
  print_all(base, base2, number, reverse_result);
  
  writeln('Nacisnij dowolny klawisz, aby zakonczyc.');
  repeat until keypressed;
end.