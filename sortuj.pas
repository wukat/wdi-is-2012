program sortuj_babelkowo;
uses crt;
const rozm = 20;
type tab = array[1..rozm] of integer;

var a : char;
    tablica : tab;

procedure wyswietl(tablica : tab);
var 
  i : integer;
begin
  for i := 1 to rozm do
    write(tablica[i],'; ');
end;

procedure generuj(var tablica : tab);
var
  i : integer;

begin
  for i := 1 to rozm do
    tablica[i] := random(3000);
end;

procedure sortuj1(var tablica : tab);
var
   zamiana, i, tymczas : integer;

begin
  zamiana := 1;
  while (zamiana > 0) do
  begin
    zamiana := 0;
    i := 1;
    while (i < rozm) do
    begin
      if (tablica[i] < tablica[i + 1]) then
      begin
        tymczas := tablica[i];
        tablica[i] := tablica[i + 1];
        tablica[i + 1] := tymczas;
        zamiana := 1;
      end;
      i := i + 1;    
    end;
  end;
  wyswietl(tablica);
  repeat until keypressed;
end;


procedure sortuj2(var tablica : tab);
var
   zamiana, i, gora, tymczas : integer;

begin
  zamiana := 1;
  while (zamiana > 0) do
  begin
    zamiana := 0;
    i := 1;
    gora := 0;
    while (i < rozm - gora) do
    begin
      if (tablica[i] < tablica[i + 1]) then
      begin
        tymczas := tablica[i];
        tablica[i] := tablica[i + 1];
        tablica[i + 1] := tymczas;
        zamiana := 1;
      end;
      i := i + 1;    
    end;
    gora := gora + 1;
  end;
  wyswietl(tablica);
  repeat until keypressed;
end;


procedure sortuj3(var tablica : tab);
var
   zamiana, i, gora, dol, tymczas : integer;

begin
  zamiana := 1;
  dol := 1;
  gora := rozm; 
  while (zamiana > -1) do
  begin
    zamiana := -1;
    i := dol;
    while (i < gora + 1) do
    begin
      if (tablica[i] < tablica[i + 1]) then
      begin
        tymczas := tablica[i];
        tablica[i] := tablica[i + 1];
        tablica[i + 1] := tymczas;
        zamiana := i;
      end
      else
        if (zamiana = -1) then dol := i + 1;
      i := i + 1;    
    end;
    if (dol > 1) then dol := dol - 1;
    gora := zamiana;
  end;
  wyswietl(tablica);
  repeat until keypressed;
end;

begin
  randomize();
  a := ' ';
  repeat
    repeat
      clrscr;
      writeln('1. Generowanie tablicy.');
      writeln('2. Sortowanie pierwsze.');
      writeln('3. Sortowanie drugie.');
      writeln('4. Sortowanie trzecie.');
      writeln('5. Zakoncz.');
  
      a := readkey();
    until (a = '1') or (a = '2') or (a = '3') or (a = '4') or (a = '5');

    case a of
      '1': generuj(tablica);
      '2': sortuj1(tablica);
      '3': sortuj2(tablica);
      '4': sortuj3(tablica);
      '5': halt;
    end;
    a := ' ';
  until 1 < 0;
  repeat until keypressed;
end.
