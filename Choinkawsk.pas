program choinka;
uses crt; 

type 
  osoba_wsk = ^osoba;
  prezent_wsk = ^prezent;
  osoba = record
    imie : string;
    nastepny : osoba_wsk;
    pierwszy : prezent_wsk;
    ostatni : prezent_wsk; 
    end;
  prezent = record
    nazwa : string;
    nastepny : prezent_wsk;
    end;

var
  korzen_pocz, korzen_koniec : osoba_wsk; 
  imie : string;
  decision : char;
  i, j : integer;


procedure wstawianie_prezentu(persona : osoba_wsk; wartosc : string);
var 
  nowy : prezent_wsk;

begin
  new(nowy);  
  nowy^.nazwa := wartosc;  
  nowy^.nastepny := NIL;  
  if persona^.pierwszy = NIL then    
    persona^.pierwszy := nowy  
  else    
    persona^.ostatni^.nastepny := nowy;  
  persona^.ostatni := nowy;
end;


procedure wstawianie_osoby(wartosc : string);
var 
  nowy : osoba_wsk;
  cos : string;
  decision : char;

begin
  decision := 'T';
  new(nowy);  
  nowy^.imie := wartosc;
  nowy^.nastepny := NIL;  
  if korzen_pocz = NIL then    
    korzen_pocz := nowy  
  else    
    korzen_koniec^.nastepny := nowy;  
  korzen_koniec := nowy;
  repeat
    writeln('Co chcesz podarowac?');
    read(cos);
    wstawianie_prezentu(korzen_koniec, cos);
    writeln('Czy chcesz podarowac cos wiecej? [T/N]');
    decision := readkey();
    readln();
  until (decision <> 'T') and (decision <> 't')
end;


procedure wyswietl_prezenty(persona : osoba_wsk);
var 
  fajnie : prezent_wsk;

begin  
  fajnie := persona^.pierwszy;  
  while (fajnie <> NIL) do  
  begin    
    write(fajnie^.nazwa,', ');    
    fajnie := fajnie^.nastepny;   
  end;  
  writeln('');
end;


procedure wyswietl_drzewo();
var 
  fajnie1 : osoba_wsk;

begin  
  fajnie1 := korzen_pocz;  
  while (fajnie1 <> NIL) do  
  begin    
    write(fajnie1^.imie, ' otrzymuje ');
    wyswietl_prezenty(fajnie1);   
    fajnie1 := fajnie1^.nastepny;   
  end;  
  repeat until keypressed;
end;

begin
  korzen_pocz := NIL;
  korzen_koniec := NIL;

  repeat    
    clrscr; 
    
    writeln('1. Dodaj osobe i jej prezenty.');
    writeln('3. Wysietl prezenty.');    
    writeln('4. Zakoncz.');    
    decision := readkey;    
    case decision of      
      '1': begin
             clrscr;
             writeln('Podaj imie:');
             readln(imie);
             wstawianie_osoby(imie);
           end;   
      '3': begin
             clrscr; 
             for i := 0 to 14 do
             begin
               for j:=(14-i) downto 1 do  
                 write(' ');         
               for j := 1 to 2*i-1 do
                 write('*');
               writeln('');
             end;
             wyswietl_drzewo();    
           end; 
    end; 
  until (decision = '4');
end.
