unit fractions;

interface

type 
  fraction = record
    co : longint;
    de : longint;
  end;


function GCD(a,b : integer) : integer;
function simplify_fr(a : fraction) : fraction;
procedure show_fr(a : fraction);
operator +(a,b : fraction) r : fraction;
operator -(a,b : fraction) r : fraction;
operator *(a,b : fraction) r : fraction;


implementation

operator +(a,b : fraction) r : fraction;
begin
  r.co := a.co * b.de + b.co * a.de;
  r.de := a.de * b.de;
end;


operator -(a,b : fraction) r : fraction;
begin
  r.co := a.co * b.de - b.co * a.de;
  r.de := a.de * b.de;
end;


operator *(a,b : fraction) r : fraction;
begin
  r.co := a.co * b.co;
  r.de := a.de * b.de;
end;

function GCD(a,b : integer) : integer;
var
  c : integer;

begin
  while (b <> 0) do
  begin
    c := a mod b;
    a := b;
    b := c;
   end;
  GCD := a;
end;

function simplify_fr(a : fraction) : fraction;
var
  divisor : integer;

begin
  divisor := GCD(a.co, a.de);
  if (divisor > 1) then
  begin
    simplify_fr.co := a.co div divisor;
    simplify_fr.de := a.de div divisor;
  end
  else
  begin
    simplify_fr.co := a.co;
    simplify_fr.de := a.de;
  end;
end;

procedure show_fr(a : fraction);
var
  int_part : longint;

begin
  a := simplify_fr(a);
  int_part := a.co div a.de;
  if (a.co < 0) and (a.de < 0) then
  begin
    a.co := -a.co;
    a.de := -a.de;
  end;
  if (int_part <> 0) then
  begin
    if (a.co - (int_part * a.de) <> 0) then
      write(int_part,' ', a.co - (int_part * a.de), '/', a.de)
    else
      write(int_part);
  end
  else
    if a.co <> 0 then
      write(a.co, '/', a.de)
    else
      write(int_part);
end; 

begin
end.

