program sortuj_babelkowo;
uses crt;
const rozm = 20;
type tab = array[1..rozm] of integer;

var a : char;
    tablica : tab;

procedure wyswietl(tablica : tab);
var 
  i : integer;
begin
  for i := 1 to rozm do
    write(tablica[i],'; ');
end;

procedure generuj(var tablica : tab);
var
  i : integer;

begin
  for i := 1 to rozm do
    tablica[i] := random(20) + 1;
end;

procedure quick_sort(var tablica : tab; p, k : integer);
var 
  i, j, x, w : integer;

begin 
  i := p;
  j := k;
  x := tablica[(p + k) div 2];
  repeat
    while tablica[i] < x do i := i + 1;
    while tablica[j] > x do j := j - 1; 
    if i <= j then 
    begin 
      w := tablica[i]; 
      tablica[i] := tablica[j]; 
      tablica[j] := w; 
      i := i + 1; 
      j := j - 1; 
    end;
  until i > j;
  if p < j then quick_sort(tablica, p, j);
  if i < k then quick_sort(tablica, i, k);
end;


procedure CountingSort(var tablica : tab; k : integer);
var 
  i : integer;
  c : tab;
  b : tab;

begin
  for i := 1 to k do c[i] := 0;
  for i := 1 to rozm do c[tablica[i]] := c[tablica[i]] + 1; 
  for i := 2 to k do c[i] := c[i] + c[i-1];
  for i := rozm downto 1 do 
  begin
    b[c[tablica[i]]] := tablica[i];
    c[tablica[i]] := c[tablica[i]] - 1;
  end;
  wyswietl(b);
end; 



function check_heap(n: integer) : boolean;
var
  i: integer;
begin
  check_heap := false;
  for i := 2 to n do
  if tablica[i] > tablica[i div 2] then exit;
  check_heap := true;
end;
 
procedure shift_down(n,i: integer);
var
  j, k : integer;
  aux : integer;
begin
  k := i;
  repeat
    j := k;
    if (2 * j <= n) and (tablica[2 * j] > tablica[k]) then
       k := 2 * j;
    if (2*j+1 <= n) and (tablica[2*j+1] > tablica[k]) then
       k := 2*j + 1;
    aux:=tablica[j];
    tablica[j] := tablica[k];
    tablica[k] := aux;
  until j = k;
end;
 
procedure build_heap;
var
  i: integer;
begin
  for i := rozm div 2 downto 1 do
    shift_down (rozm,i);
end;
 
procedure sort;
var
  i : integer;
  aux : integer;
begin
  for i := rozm downto 2 do
  begin
    aux := tablica[1];
    tablica[1] := tablica[i];
    tablica[i] := aux;
    shift_down(i-1, 1);
    Assert(check_heap(i-1));
  end;
end;

procedure kopcowanie(var tablica : tab);
begin
  build_heap;
  Assert(check_heap(rozm));
  sort;
end;

procedure sortuj4(var tablica : tab);
var
   zamiana, i, tymczas : integer;

begin
end;


begin
  randomize();
  a := ' ';
  repeat
    repeat
      clrscr;
      writeln('1. Generowanie tablicy.');
      writeln('2. Quick sort.');
      writeln('3. CountingSort.');
      writeln('4. Sortowanie trzecie.');
      writeln('5. Sortowanie czwarte.');
      writeln('6. Zakoncz.');
  
      a := readkey();
    until (a = '1') or (a = '2') or (a = '3') or (a = '4') or (a = '5') or (a = '6');

    case a of
      '1': generuj(tablica);
      '2': begin quick_sort(tablica, 1, rozm); wyswietl(tablica); repeat until keypressed; end;
      '3': begin CountingSort(tablica, 20); repeat until keypressed; end;
      '4': begin kopcowanie(tablica); wyswietl(tablica); repeat until keypressed; end;
      '5': sortuj4(tablica);
      '6': halt;
    end;
    a := ' ';
  until 1 < 0;
  repeat until keypressed;
end.
