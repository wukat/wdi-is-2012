program kolejka_a_pointer;
uses crt;

type 
  kolejka_pointer=^kolejka;
  kolejka = record
    nastepny : kolejka_pointer;
    zawartosc : integer;
  end;

var poczatek, koniec : kolejka_pointer;
  decision : char;


procedure dodaj();
var nowy : kolejka_pointer;
    wartosc : integer;

begin
  writeln('Jaka wartosc chcesz dodac?');
  writeln('Podaj liczbe calkowita:');
  read(wartosc);
  new(nowy);
  nowy^.zawartosc := wartosc;
  nowy^.nastepny := NIL;
  if poczatek = NIL then
    poczatek := nowy
  else
    koniec^.nastepny := nowy;
  koniec := nowy;
end;


procedure usun();
var usuwany : kolejka_pointer;

begin
  if (poczatek^.nastepny <> NIL) then
  begin
    usuwany := poczatek;
    poczatek := poczatek^.nastepny;
    dispose(usuwany);
  end
  else 
  begin
    dispose(poczatek);
    poczatek := NIL;
  end;  
end;


procedure wyswietl();
var fajnie: kolejka_pointer;

begin
  fajnie := poczatek;
  while (fajnie^.nastepny <> NIL) do
  begin
    writeln(fajnie^.zawartosc);
    fajnie := fajnie^.nastepny; 
  end;
  writeln(fajnie^.zawartosc);
  repeat until keypressed;
end;


begin
  poczatek := NIL;
  koniec := NIL;
  repeat
    clrscr;
    writeln('1. Dodaj element na koniec kolejki.');
    writeln('2. Usun element z kolejki (pierwszy).');
    writeln('3. Wysietl kolejke.');
    writeln('4. Zakoncz.');

    decision := readkey;
    case decision of
      '1': dodaj();
      '2': usun();
      '3': wyswietl();
    end;
  until (decision = '4');
end.
