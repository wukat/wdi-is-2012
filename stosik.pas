program stos_a_pointer;
uses crt;

type 
  stos_pointer=^stos;
  stos = record
    dolny : stos_pointer;
    zawartosc : integer;
  end;

var wierzch : stos_pointer;
  decision : char;


procedure dodaj();
var nowy : stos_pointer;
    wartosc : integer;

begin
  writeln('Jaka wartosc chcesz dodac?');
  writeln('Podaj liczbe calkowita:');
  read(wartosc);
  new(nowy);
  nowy^.dolny := wierzch;
  nowy^.zawartosc := wartosc;
  wierzch := nowy;
end;


procedure usun();
var usuwany : stos_pointer;

begin
  if (wierzch^.dolny <> NIL) then
  begin
    usuwany := wierzch;
    wierzch := wierzch^.dolny;
    dispose(usuwany);
  end
  else
  begin
    dispose(wierzch);
    wierzch := NIL;
  end;
end;


procedure wyswietl();
var fajnie: stos_pointer;

begin
  fajnie := wierzch;
  while (fajnie^.dolny <> NIL) do
  begin
    writeln(fajnie^.zawartosc);
    fajnie := fajnie^.dolny; 
  end;
  writeln(fajnie^.zawartosc);
  repeat until keypressed;
end;


begin
  wierzch := NIL;
  repeat
    clrscr;
    writeln('1. Dodaj element do stosu.');
    writeln('2. Usun element ze stosu (gorny).');
    writeln('3. Wysietl stos.');
    writeln('4. Zakoncz.');

    decision := readkey;
    case decision of
      '1': dodaj();
      '2': usun();
      '3': wyswietl();
    end;
  until (decision = '4');
end.
