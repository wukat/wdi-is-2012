unit matrx;

interface
type
  matrix = array of array of integer;

function fill_matrix(n, m : integer) : matrix;
procedure show_matrix(var mtrx : matrix; n, m : integer);
function add_matrices(var a, b : matrix; n, m : integer) : matrix;
function subtract_matrices(var a, b : matrix; n, m : integer) : matrix;
function multiply_matrix_by_scalar(var a : matrix; scalar, n, m : integer) : matrix;
function det(var a : matrix; n, k : integer) : integer;
function multiply_matrices(var a : matrix; var b : matrix; k, l, m : integer) : matrix;
function inverse_matrix(var a : matrix; n, k : integer) : matrix;

implementation


function fill_matrix(n, m : integer) : matrix;
var
  i, j : integer;
  new_matrix : matrix;

begin
  setlength(new_matrix, n, m);
  for i := 0 to n - 1 do
  begin
    for j := 0 to m - 1 do
    begin
      write('[',i + 1,';',j + 1,']: ');
      read(new_matrix[i][j]);
    end;
    writeln('');
  end;
  fill_matrix := new_matrix;
end;


procedure show_matrix(var mtrx : matrix; n, m : integer);
var
  i, j : integer;

begin
  for i := 0 to n - 1 do
  begin
    for j := 0 to m - 1 do
    begin
      write(' [',i + 1,';',j + 1,']: ');
      write(mtrx[i][j]);
    end;
    writeln('');
  end;
end;


function add_matrices(var a, b : matrix; n, m : integer) : matrix;
var
  i, j : integer;
  c : matrix;

begin
  setlength(c, n, m);
  for i := 0 to n - 1 do
  begin
    for j := 0 to m - 1 do
    begin
      c[i,j] := a[i][j] + b[i][j];
    end;
    writeln('');
  end;
  add_matrices := c;
end;


function subtract_matrices(var a, b : matrix; n, m : integer) : matrix;
var
  i, j : integer;
  c : matrix;

begin
  setlength(c, n, m);
  for i := 0 to n - 1 do
  begin
    for j := 0 to m - 1 do
    begin
      c[i,j] := a[i][j] - b[i][j];
    end;
    writeln('');
  end;
  subtract_matrices := c;
end;


function multiply_matrix_by_scalar(var a : matrix; scalar, n, m : integer) : matrix;
var
  i, j : integer;
  c : matrix;

begin
  setlength(c, n, m);
  for i := 0 to n - 1 do
  begin
    for j := 0 to m - 1 do
    begin
      c[i,j] := a[i][j] * scalar;
    end;
    writeln('');
  end;
  multiply_matrix_by_scalar := c;
end;

function det(var a : matrix; n, k : integer) : integer;
var 
  i, j, check, temp : integer;

begin
  check := 0;
  i := 1;
  if k < n then 
  begin
    if a[k - 1][k - 1] = 0 then
    begin
      while (check = 0) and (i <= n - k + 1) do
      begin
        if a[i][k - 1] <> 0 then    
        begin
          for j := k - 1 to n - 1 do 
          begin
            a[k - 1][j] := a[k - 1][j] - a[i][j];
          end;
          check := 1;
        end;
        i := i + 1;
      end;
    end;

    if (check = 0) and (i = n) then det := 0
    else
    begin
      for i := k to n - 1 do
      begin
        temp := a[i][k - 1] div a[k - 1][k - 1];
        for j := k - 1 to n - 1 do
          a[i][j] := a[i][j] - temp * a[k - 1][j];
      end;
      show_matrix(a, n, n);
      det := det(a, n, k + 1) * a[k - 1][k - 1];
    end;
  end
  else
  det := a[n - 1][n - 1];
end;


function multiply_matrices(var a : matrix; var b : matrix; k, l, m : integer) : matrix;
var 
  i1, i2, i3 : integer;
  new_matrix : matrix;

begin
  setlength(new_matrix, k, l);
  for i1 := 0 to k - 1 do
    for i2 := 0 to l - 1 do
      for i3 := 0 to m - 1 do
        new_matrix[i1, i2] := new_matrix[i1, i2] + a[i1, i3]*b[i3, i2];
  multiply_matrices := new_matrix;
end;


function inverse_matrix(var a : matrix; n, k : integer) : matrix;
var 
  i, j, check, temp, dividor : integer;

begin
  check := 0;
  i := 1;
  if k < n then 
  begin
    if a[k - 1][k - 1] = 0 then
    begin
      while (check = 0) and (i <= n - k + 1) do
      begin
        if a[i][k - 1] <> 0 then    
        begin
          for j := k - 1 to n - 1 do 
          begin
            temp := a[k - 1][j];
            a[k - 1][j] := a[i][j];
            a[i][j] := temp;
            temp := U[k - 1][j];
            U[k - 1][j] := U[i][j];
            U[i][j] := temp;
          end;
          check := 1;
        end;
        i := i + 1;
      end;
    end;

    dividor := a[k - 1][k - 1];
    a[k - 1][k - 1] := 1;
    for j := k to n - 1 do
      a[k - 1][j] := a[k - 1][j] div dividor;
    for j := k - 1 to n - 1 do
      U[k - 1][j] := U[k - 1][j] div dividor;
    
    for i := k to n - 1 do
    begin
      dividor := a[i][k - 1] div a[k - 1][k - 1];
      for j := k - 1 to n - 1 do
        a[i][j] := a[i][j] - dividor * a[k - 1][j];
    end;
    a := inverse_matrix(a, n, k + 1);
    
  end;
  
  inverse_matrix := U;
end;
end;

begin

end.

