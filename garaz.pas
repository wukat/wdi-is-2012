program garaz;
uses crt;

const n = 3;
type tab = array[1..n] of integer;

var 
  number_of_places, count_cars : tab;
  free_places, taken : word;


function input_integer(min, max : integer):integer;
var
  input : string;
  error, number : integer;

begin
  repeat
    readln(input);
    val(input,number,error);
    if (number < min) or (number > max) then 
      begin 
        error := 1;
        writeln('Blad! Sprobuj ponownie!');
      end;
  until error = 0;
  input_integer := number;
end;


function make_decision(weight : integer) : byte;
var
  decision : byte;

begin
  if weight > 10 then
    decision := 1
  else
    if weight > 5 then
      decision := 2
    else
      decision := 3;
  make_decision := decision;
end;


procedure parking(var count_cars, number_of_places : tab; decision : byte);
var
  i : integer;

begin
  if number_of_places[decision] > 0 then
    begin
      number_of_places[decision] := number_of_places[decision] - 1;
      writeln('Zaparkowano na poziomie ', decision, '!');
      count_cars[decision] := count_cars[decision] + 1;
    end
  else
    begin
      decision := decision - 1;
      if decision < 1 then
        writeln('Przepraszamy, nie ma wolnych miejsc dla pojazdu o takiej wadze.')
      else
         parking(count_cars, number_of_places, decision);
    end;
end;


procedure enter_parking(var count_cars, number_of_places : tab);
var
  weight : integer;
  decision : byte;

begin
  writeln('Podaj wage samochodu (zaokraglij, w tonach).');
  weight :=  input_integer(1, 60);
  decision := make_decision(weight);
  parking(count_cars, number_of_places, decision);
end;


procedure leave_parking(var count_cars, number_of_places : tab);
var 
  level : integer;

begin
  write('Na ktorym poziomie parkujesz?  ');
  level := input_integer(1, n);
  if count_cars[level] > 0 then 
    begin
      writeln('Opuszczasz poziom ', level, '!');
      number_of_places[level] := number_of_places[level] + 1;
      count_cars[level] := count_cars[level] - 1;
    end
  else
    writeln('Na tym poziomie nie ma samochodow!');
end;
  
procedure menu(number_of_places, count_cars : tab; free_places, taken : word; counter : integer);
var 
    
    choosen, i : word;

begin
  if counter = 0 then
    begin
      taken := 0; 
      free_places := 0;
      for i := 1 to n do
        begin
          number_of_places[i] := 10;
          free_places := free_places + number_of_places[i];
          count_cars[i] := 0;
        end;
    end;
  
  counter := counter + 1;
  
  writeln('Nacisnij m, aby przejsc do menu');
  repeat until readkey = 'm';
  clrscr;
  writeln('Wybierz dzialanie:');
  writeln('[1] Wjedz na parking');
  writeln('[2] Opusc parking');
  writeln('[3] Sprawdz stan parkingu');
  writeln('[4] Wyjsice');
  choosen := input_integer(1,4);
  if choosen = 1 then
    begin
      clrscr;
      if free_places > 0 then
        begin
          enter_parking(count_cars, number_of_places);
          free_places := free_places - 1;
          taken := taken + 1;
        end
      else
        writeln('Nie ma wolnych miejsc, przepraszamy.');
      menu(number_of_places, count_cars, free_places, taken, counter);
    end
  else
    if choosen = 2 then
      begin
        clrscr;
	if taken > 0 then
          begin
	    leave_parking(count_cars, number_of_places);
            free_places := free_places + 1;
            taken := taken - 1;
          end
	else
          writeln('Na parkingu nie ma samochodow!');
        menu(number_of_places, count_cars, free_places, taken, counter);
      end
    else     
      if choosen = 3 then
        begin
          clrscr;
          writeln('Poziom   Liczba samochodow   Liczba miejsc wolnych');
	  for i := 1 to n do
            writeln(i, '                 ', count_cars[i]:2, '                    ', number_of_places[i]:2);
	  menu(number_of_places, count_cars, free_places, taken, counter);
        end
      else
        halt;
end;

begin
  clrscr;
  menu(number_of_places, count_cars, free_places, taken, 0);
  
end.