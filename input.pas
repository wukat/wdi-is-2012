unit input;


interface

uses fractions, complex;
function input_integer() : integer;
function input_integer_mm(min, max : integer) : integer;
function input_fraction() : fraction;
function input_complex(SoN : char) : complex_number;


implementation

function input_integer() : integer;
var
  input : string;
  error : integer;
  output : integer;

begin
  error := 1;
  while (error <> 0) do
    begin
      readln(input);
      val(input, output, error);
      if (error <> 0) then 
        writeln('Podales zle dane, wpisz ponownie');
    end;
  input_integer := output;
end;


function input_integer_mm(min, max : integer) : integer;
var
  input : string;
  error, output : integer;
begin
  error := 1;
  write('podaj liczbe calkowita z przedzialu: [', min,';', max, ']: ');
  while (error <> 0) do
    begin
      readln(input);
      val(input,output,error);
      if ((output > max) or (output < min)) then 
        error := 1;
      if (error <> 0) then 
        writeln('Podales zle dane, wpisz ponownie. Liczba ma nalezec do przedzialu [', min,';', max, '].');
    end;    
  input_integer_mm := output;
end;

function input_fraction() : fraction;
var
  a : integer;

begin
  writeln('Podaj czesc calkowita:');
  a := input_integer();
  writeln('Podaj licznik ulamka:');
  input_fraction.co := input_integer();
  writeln('Podaj mianownik ulamka (rozny od zera):');
  repeat
    input_fraction.de := input_integer();
    if (input_fraction.de = 0) then writeln('Podaj mianownik rozny od 0!');
  until input_fraction.de <> 0;
  input_fraction.co := input_fraction.co + (input_fraction.de * a);
end;

function input_complex(SoN : char) : complex_number;
begin
  write('Podaj czesc rzeczywista, ');
  input_complex.re := input_fraction();
  if (SoN = 'c') then
  begin
    write('Podaj czesc urojona (wspolczynnik przy i),  ');
    input_complex.im := input_fraction();
  end
  else
    input_complex.im.co := 0;
end;



begin
end.

