program gra;
uses crt, graph, sysutilsm, dos;

const
  bricks_width = 60;
  bricks_height = 15;
  bullet_color = green;
  points_per_one = 10;
  background = black;
  R = 5;

type
  strings = array [1..8] of string;
  results = array [1..5] of integer;

  bonus = object     {bonus}
    x : integer;
    y : integer;
    alive : integer;
    used : integer;
    time_start : double;
    during_time : word;
    number_of_bonus : integer;
  end;
  bonuses = array [1..8] of bonus;


  brick = object
    x : integer;
    y : integer;
    visible : integer;
    bon : bonus;
    lifes : integer;
    color : integer;
    procedure put;
  end;
  wall = array [1..8] of array [1..6] of brick;


  ball = object
    x : real;
    y : real;
    xp : real;       {wspolrzedne poprzedniego ustawienia}
    yp : real;
    speed : integer;
    turn_x : integer;
    turn_y : integer;
    angle : integer;
    sticked : integer;
    procedure show(color : integer);
    procedure bounce_horizontal;
    procedure bounce_vertical;
    procedure move(var bricks : wall; var points : integer);
  end;


  missle = object
    x : integer;
    y : integer;
    xp : integer;        {wspolrzedne poprzedniego ustawienia}
    yp : integer;
    alive : integer;
    procedure print(color : integer);
  end;


  tray = object
    width : integer;
    height : integer;
    x : integer;
    y : integer;
    xp : integer;        {wspolrzedne poprzedniego ustawienia}
    yp : integer;
    speed : integer;
    turn : integer;
    sticky : integer;
    fire_mode : integer;
    procedure bounce(var bullet : ball; var lifes : integer);
    procedure draw(color : integer);
    procedure move(var bullet : ball; c : char);
    function shoot : missle;
    procedure change_width(w : integer);
  end;

var
  board : integer;
  ts : real;

{inicjalizacja grafiki}
procedure graphic();
var
  tryb, ster : smallint;
begin
  ster := VGA; tryb := 2;
  InitGraph(ster, tryb,'');
end;


{zwraca czas systemowy w sekundach}
function get_time() : real;
var
  a,b,c,d : word;
  secs : real;
  temp : double;
begin
  secs := 0.0;
  temp := time;
  decodetime(temp, a, b, c, d);
  secs := 3600 * a + b * 60 + c;
  secs := secs + d/1000;
  get_time := secs;
end;

{wyswietlanie punktow}
procedure actual_state_p(points : integer);
var                                         
  temp : string;                            
begin                                       
  setfillstyle(1,black);                    
  bar(600,60,640,74);
  setcolor(white);                          
  temp := inttostr(points);                 
  outtextxy(600,60, temp);
end;

{wyswietlanie liczby pozostalych zyc}
procedure actual_state_l(lifes : integer);
var
  temp : string;
begin
  setfillstyle(1,black);
  bar(600,45,640,59);
  setcolor(white);        
  temp := inttostr(lifes);
  outtextxy(600,45, temp);
end;

{-------------------------------BRICK------------------------------------------}
{wyswietlanie pojedynczego}
procedure brick.put;
begin
  if (lifes = 1) and (bon.number_of_bonus > 0) and (bon.alive = 1) then  {wyswietlamy niebieski bonus, jesli jest i klocek zostal zbity}
  begin
    setfillstyle(1, 9);
    bar(x, y, x + bricks_width - 1, y + bricks_height - 1);
    setcolor(black);
    outtextxy(x + 25, y + 5, 'B');
    visible := 1;
  end
  else
  if (lifes > 0) then {jesli klocek nie zostal zbity wyswietlamy}
  begin
    if lifes = 1 then
      setfillstyle(1, color);
    if lifes > 1 then
      setfillstyle(1, red + lifes); {kolor w zaleznosci od liczby zyc}
    bar(x, y, x + bricks_width -1, y + bricks_height - 1);
  end;
  if (lifes = 0) and (visible = 1) then {tlo dla martwego}
  begin
    setfillstyle(0, background);
    bar(x, y, x + bricks_width - 1, y + bricks_height - 1);
    visible := 0;
  end;
end;


{--------------------------------BALL------------------------------------------}
{odbicie od osi pionewej}
procedure ball.bounce_vertical;
begin
  turn_x := -turn_x;
end;

{odbicie od osi poziomej}
procedure ball.bounce_horizontal;
begin
  turn_y := -turn_y;
end;

{wyswietlanie}
procedure ball.show(color : integer);
begin
  setcolor(background); {zamalowujemy poprzednie polozenie}
  Setfillstyle(1,background);
  pieslice(round(xp),round(yp),0,360,R);
  xp := x;             {przepisujemy wspolrzedne obecnego do poprzedniego}
  yp := y;
  setcolor(color);    {wyswietlamy aktualny}
  Setfillstyle(1,color);
  pieslice(round(x),round(y),0,360,R);
end;

{ruch pilki ze zbijaniem klockow i odbiciami}
procedure ball.move(var bricks : wall; var points : integer);
var
  i,j : integer;
  boun : integer;
  temp_i, temp_j : integer;  {wsporzedne zbitego klocka}
begin
  y := y + turn_y * sin(angle * 2 * Pi/360) * speed;    {obliczamy wspolrzedne}
  x := x + turn_x * cos(angle * 2 * Pi/360) * speed;

  temp_i := -1;
  if (x <= 6) then  {sprawdzamy czy trzeba odbic od sciany}
  begin
    x := 7;
    bounce_vertical;
    boun := 1;
  end
  else
  if x >= 484 then
  begin
    x := 483;
    bounce_vertical;
    boun := 1;
  end;

  if (y <= 6) then   {albo sufitu}
  begin
    bounce_horizontal;
    boun := 1;
  end;


  boun := 0;
  for i := 1 to 8 do  {dla kazdego klocka sprawdzamy 4 mozliwosci, jesli wczesniej nie bylo odbicia}
    for j := 1 to 6 do
    begin
      if (boun = 0) and (bricks[i][j].lifes > 0) then
      begin
        if (x < bricks[i][j].x + 64) and (x > bricks[i][j].x - 6) and (y < bricks[i][j].y + 21) and (y > bricks[i][j].y - 6) then  {czy kulka jest w klocku + promien klulki}
        begin
          if (i > 1) and (x < bricks[i][j].x) and (bricks[i - 1][j].lifes = 0) and (turn_x = 1) then {odbicie z lewej strony}
          begin
            temp_i := i;  {zapisujemy ktory to klocek}
            temp_j := j;
            boun := 1;
            bounce_vertical;                          {odbijamy}
          end;
          if (boun = 0) and (i < 8) and (x > bricks[i][j].x + 58) and (bricks[i + 1][j].lifes = 0) and (turn_x = -1) then  {odbicie z prawej}
          begin
            temp_i := i;
            temp_j := j;
            boun := 1;
            bounce_vertical;
          end;
          if (boun = 0) and (y > bricks[i][j].y + 15) and (turn_y = -1) then  {odbicie od dolu}
          begin
            temp_i := i;
            temp_j := j;
            boun := 1;
            bounce_horizontal;
          end;
          if (boun = 0) and (y < bricks[i][j].y) and (turn_y = 1) then  {odbicie od gory}
          begin
            temp_i := i;
            temp_j := j;
            boun := 1;
            bounce_horizontal;
          end;
        end;
      end;
    end;

  if (boun = 1) and (temp_i <> -1) then           {jesli zbito klocek}
  begin
    bricks[temp_i][temp_j].lifes := bricks[temp_i][temp_j].lifes - 1; {to go zerujemy}
    bricks[temp_i][temp_j].put;
    points := points + points_per_one;   {naliczamy i aktualizujemy punkty}
    actual_state_p(points);
  end;
end;


{analogiczna funkcja dla CPU, bez zbijania klockow}
procedure move_comp(var bullet : ball; bricks : wall);
var
  i,j : integer;
  boun : integer;
begin
  bullet.y := bullet.y + bullet.turn_y * sin(bullet.angle * 2 * Pi / 360) * bullet.speed;
  bullet.x := bullet.x + bullet.turn_x * cos(bullet.angle * 2 * Pi / 360) * bullet.speed;
  if (bullet.x <= 6) then
  begin
    bullet.x := 7;
    bullet.bounce_vertical;
  end
  else
  if bullet.x >= 484 then
  begin
    bullet.x := 483;
    bullet.bounce_vertical;
  end;

  if (bullet.y <= 6) then
  begin
    bullet.bounce_horizontal;
  end;

  boun := 0;
  for i := 1 to 8 do
    for j := 1 to 6 do
    begin
      if (boun = 0) and (bricks[i][j].lifes > 0) then
      begin
        if (bullet.x < bricks[i][j].x + 64) and (bullet.x > bricks[i][j].x - 6) and (bullet.y < bricks[i][j].y + 21) and (bullet.y > bricks[i][j].y - 6) then
        begin
          if (i > 1) and (bullet.x < bricks[i][j].x) and (bricks[i - 1][j].lifes = 0) and (bullet.turn_x = 1) then
          begin
            boun := 1;
            bullet.bounce_vertical;
          end;
          if (boun = 0) and (i < 8) and (bullet.x > bricks[i][j].x + 58) and (bricks[i + 1][j].lifes = 0) and (bullet.turn_x = -1) then
          begin
            boun := 1;
            bullet.bounce_vertical;
          end;
          if (boun = 0) and (bullet.y > bricks[i][j].y + 15) and (bullet.turn_y = -1) then
          begin
            boun := 1;
            bullet.bounce_horizontal;
          end;
          if (boun = 0) and (bullet.y < bricks[i][j].y) and (bullet.turn_y = 1) then
          begin
            boun := 1;
            bullet.bounce_horizontal;
          end;
        end;
      end;
    end;
end;

{--------------------------------MISSLE----------------------------------------}
{wyswietlanie}
procedure missle.print(color : integer);
begin
  if (alive = 1) then
  begin
    SetFillStyle(1, background);  {zamalowujemy stary}
    Bar(xp, yp, xp + 2, yp - 8);
    xp := x;         {przepisujemy wspolrzedne}
    yp := y;
    SetFillStyle(1, color);    {rysujemy nowe}
    Bar(x, y, x + 2, y - 8);
  end;
  if (alive = 0) then  {dla nieaktywnego zamalowujemy}
  begin
    SetFillStyle(1, background);
    Bar(xp, yp, xp + 2, yp - 8);
    xp := 800;
    yp := 800;
  end;
end;

{zestrzeliwanie klockow}
procedure fire(var missles : missle; var bricks : wall);
var
  j, k : integer;
begin
    if missles.alive = 1 then
    begin
      missles.y := missles.y - 8; {uaktualnienie polozenia}
      for k := 1 to 8 do                                   {sprawdzamy wszystkie klocki}
        for j := 6 downto 1 do
        begin
          if (bricks[k][j].lifes > 0) and (bricks[k][j].x <= missles.x) and (bricks[k][j].x + bricks_width > missles.x) and
          (bricks[k][j].y + bricks_height >= missles.y) and (bricks[k][j].y <= missles.y) then
          begin
            bricks[k][j].lifes := bricks[k][j].lifes - 1;  {i ewentualnie zabijamy}
            missles.alive := 0;   {jesli zbity to pocisk nieaktywny}
            missles.print(blue);
          end;
        end;
      if (missles.alive = 1) and (missles.y < 0) then      {jesli poza mapa to tez nie}
      begin
        missles.alive := 0;
        missles.print(blue);
      end;
    end;
end;


{-------------------------------TRAY-------------------------------------------}
{wyswietlanie}
procedure tray.draw(color : integer);
var
  i : integer;
begin
  SetFillStyle(1, background);
  {zamalowujemy tylko fragment, ten ktory sie zmienil}
  if xp < x then
    bar(xp - height, y, x, y + height)
  else
  if xp > x then
    bar(x + width, y, xp + width + height, y + height);
  xp := x;                          {przepisujemy wpsolrzedna}
  {rysujemy tacke w nowym polozeniu}
  SetColor(color);
  SetFillStyle(1, color);
  pieslice(x, y + height, 180,90, height);
  pieslice(x + width, y + height, 0, 90, height);
  for i := y to y + height do
    line(x, i, x + width, i);
  {jesli w trybie strzelania, to rysujemy dzialko}
  if (fire_mode = 1) then
  begin
    setcolor(background);
    for i := y to y + 5 do
      line(x + (width div 2) - 5, i, x + (width div 2) + 5, i);
    setcolor(color);
    for i := y + 5 to y + height do
      line(x + (width div 2) - 5, i, x + (width div 2) + 5, i);
    circle(x + (width div 2), y + 5, 5);
  end;
end;

{poruszanie tacki}
procedure tray.move(var bullet : ball; c : char); {w zaleznosci od wcisnietego klawisza}
begin
  turn := 0;           {ustalamy w ktora strone}
  if (c = 'd') then
      turn := 1;
  if (c = 'a') then
    turn := -1;
  if ((x + width + height >= 490 - speed) and (turn = -1)) or ((x - height <= 1 + speed)
  and (turn = 1)) or ((x + width + height < 490 - speed) and (x - height > 1 + speed)) then  {jesli nie dotyka scian}
  begin
    x := x + speed * turn;    {to zmieniamy wspolrzedne}
    if bullet.sticked = 1 then   {i jesli kulka jest przyklejona, jej te�}
    begin
      bullet.x := bullet.x + speed * turn;
      if (bullet.x >= 484) then bullet.x := 484;
      if (bullet.x <= 6) then bullet.x := 6;
    end;
  end;
end;

{strzelanie}
function tray.shoot : missle;
var
  miss : missle;
begin
  miss.x := x + (width div 2);  {wyrzucamy pocisk}
  miss.y := 399;
  miss.alive := 1;
  shoot := miss;
end;

{odbijanie kulki}
procedure tray.bounce(var bullet : ball; var lifes : integer);
var
  boun : integer;
begin
  if (bullet.y > 395) then
  begin
    boun := 0;
    if (bullet.x > x + width) and (bullet.x <= x + width + height + 5) then  {jesli uderza w zaokraglone, to odlatuje zawsze pod 45 stopni}
    begin
      bullet.y := 394;
      bullet.angle := 45;
      bullet.turn_y := -1;
      bullet.turn_x := 1;
      boun := 1;
    end;
    if (bullet.x >= x - height - 5) and (bullet.x < x) then
    begin
      bullet.y := 394;
      bullet.angle := 45;
      bullet.turn_y := -1;
      bullet.turn_x := -1;
      boun := 1;
    end;
    if (bullet.x >= x + width div 2 - width div 10) and (bullet.x <= x + width div 2 + width div 10) then  {jesli blisko srodka to kat sie nie zmienia}
    begin
      bullet.y := 394;
      bullet.bounce_horizontal;
      boun := 1;
    end;
    if (bullet.x > x) and (bullet.x < x + width div 2 - width div 10) then  {jesli dalej od srodka to o 5 stopni, zlaeznie z ktorej strony przyleciala kulka}
    begin
      bullet.y := 394;
      if (bullet.turn_x = 1) and (bullet.angle < 65) then
        bullet.angle := bullet.angle + 5;
      if (bullet.turn_x = -1) and (bullet.angle > 25) then
        bullet.angle := bullet.angle - 5;
      bullet.bounce_horizontal;
      boun := 1;
    end;
    if (bullet.x > x + width div 2 + width div 10) and (bullet.x < x + width) then
     begin
      bullet.y := 394;
      if (bullet.turn_x = -1) and (bullet.angle < 65) then
        bullet.angle := bullet.angle + 5;
      if (bullet.turn_x = 1) and (bullet.angle > 25) then
        bullet.angle := bullet.angle - 5;
      bullet.bounce_horizontal;
      boun := 1;
    end;
    if (boun = 1) and (sticky = 1) then bullet.sticked := 1; {przyklejamy kulke jesli tacka klei}
    if (boun = 0) and (bullet.y > 410) then  {jesli nie udalo sie odbi�, to odbieramy zycie}
      lifes := lifes - 1;
  end;
end;

{zmiana szerokosci tacki}
procedure tray.change_width(w : integer);
begin
  setfillstyle(0,background);
  bar(x - height, y, x + width + height, y + height);
  width := w;
  if (x + width + height >= 490 - speed) then
    x :=  490 - speed - width - height;
end;

{deaktywacja bonusow, zarowno aktywnych jak tych do zbicia}
procedure deactivate_bonuses(var bricks : wall; var bonuss : bonuses; var tr : tray; var rapidity : integer);
var
  i, j : integer;
  compare, s : double;
  a, b, sec, d : word;
begin
  for i := 1 to 8 do         {sprawdzamy bonusy ktore sa do zbicia}
    for j := 1 to 6 do
    if (bricks[i][j].lifes = 1) and (bricks[i][j].bon.alive = 1) then
    begin
      s := time;
      compare := s - bricks[i][j].bon.time_start;
      decodetime(compare, a, b, sec, d);
      if (a * 3600 + b * 60 + sec > bricks[i][j].bon.during_time) then {i jesli dluzej niz during_time to je kasujemy}
      begin
        bricks[i][j].lifes := 0;
        bricks[i][j].visible := 1;
        bricks[i][j].bon.number_of_bonus := 0;
      end;
    end;
  for i := 1 to 8 do  {sprawdzamy ogolne bonusy}
  begin
    if (bonuss[i].alive = 1) then  {jesli aktywne}
    begin
      s := time;
      compare := s - bonuss[i].time_start;
      decodetime(compare, a, b, sec, d);
      if (a * 3600 + b * 60 + sec > bonuss[i].during_time) or (bonuss[i].used = 0) then  {jesli dziala dluzej niz zadany czas}
        bonuss[i].used := 0; {to jest zuzyty}
      if (bonuss[i].used = 0) then  {i kasujemy jego dzialanie}
      begin
        case i of
          3,4:  begin
                  tr.change_width(80);
                end;
          5, 6:  begin
                   rapidity := 2;
                 end;
          7:  begin
                tr.fire_mode := 0;
              end;
          8:  begin
                tr.sticky := 0;
              end;
        end;
        bonuss[i].alive := 0;  {na koniec zabijamy}
      end;
    end;
  end;
end;

{dzialanie na klocku, jesli ma 0 zyc}
procedure destroy(var tile : brick; var tr : tray; var lifes : integer; var points : integer; var rapidity : integer; var bonuss : bonuses);
begin
  if tile.lifes = 0 then
  begin
    if (tile.bon.number_of_bonus > 0) and (tile.bon.alive = 0) and (tile.bon.used = 0) then  {jesli ma w sobie bonusa, to zwracamy mu zycie i dajemy czas staru i 8 sekund do zgasniecia}
    begin
      tile.bon.alive := 1;
      tile.bon.time_start := time;
      tile.bon.during_time := 8;
      tile.lifes := 1;
    end
    else
    begin
      if (tile.bon.number_of_bonus > 0) and (tile.bon.alive = 1) and (tile.bon.used = 0) then {jesli wlasnie zbilismy bonusa}
      begin
        case tile.bon.number_of_bonus of {to bierzemy jego dzialanie}
          1:  begin                                {bonusy znosza sie wzajemnie}
                lifes := lifes + 1;
                actual_state_l(lifes);
              end;
          2:  begin
                points := points + 50;
                actual_state_p(points);
              end;
          3:  begin
                tr.change_width(50);
                tr.fire_mode := 0;
                tr.sticky := 0;
                setfillstyle(1,background);
                bar(500, 85 + (4 * 15), 640, 84 + ((4 - 1) * 15));
                bonuss[4].used := 0;
                bar(500, 85 + (7 * 15), 640, 84 + ((7 - 1) * 15));
                bonuss[7].used := 0;
                bar(500, 85 + (8 * 15), 640, 84 + ((8 - 1) * 15));
                bonuss[8].used := 0;
              end;
          4:  begin
                tr.change_width(110);
                tr.fire_mode := 0;
                tr.sticky := 0;
                setfillstyle(1,background);
                bonuss[3].used := 0;
                bar(500, 85 + (3 * 15), 640, 84 + ((3 - 1) * 15));
                bonuss[7].used := 0;
                bar(500, 85 + (7 * 15), 640, 84 + ((7 - 1) * 15));
                bonuss[8].used := 0;
                bar(500, 85 + (8 * 15), 640, 84 + ((8 - 1) * 15));
              end;
          5:  begin
                tr.change_width(80);
                rapidity := 3;
                tr.fire_mode := 0;
                tr.sticky := 0;
                setfillstyle(1,background);
                bonuss[3].used := 0;
                bar(500, 85 + (3 * 15), 640, 84 + ((3 - 1) * 15));
                bonuss[4].used := 0;
                bar(500, 85 + (4 * 15), 640, 84 + ((4 - 1) * 15));
                bonuss[6].used := 0;
                bar(500, 85 + (6 * 15), 640, 84 + ((6 - 1) * 15));
                bonuss[7].used := 0;
                bar(500, 85 + (7 * 15), 640, 84 + ((7 - 1) * 15));
                bonuss[8].used := 0;
                bar(500, 85 + (8 * 15), 640, 84 + ((8 - 1) * 15));
              end;
          6:  begin
                tr.change_width(80);
                rapidity := 1;
                tr.fire_mode := 0;
                tr.sticky := 0;
                setfillstyle(1,background);
                bonuss[3].used := 0;
                bar(500, 85 + (3 * 15), 640, 84 + ((3 - 1) * 15));
                bonuss[4].used := 0;
                bar(500, 85 + (4 * 15), 640, 84 + ((4 - 1) * 15));
                bonuss[5].used := 0;
                bar(500, 85 + (5 * 15), 640, 84 + ((5 - 1) * 15));
                bonuss[7].used := 0;
                bar(500, 85 + (7 * 15), 640, 84 + ((7 - 1) * 15));
                bonuss[8].used := 0;
                bar(500, 85 + (8 * 15), 640, 84 + ((8 - 1) * 15));
              end;
          7:  begin
                tr.fire_mode := 1;
                tr.change_width(80);
                setfillstyle(1,background);
                bonuss[3].used := 0;
                bar(500, 85 + (3 * 15), 640, 84 + ((3 - 1) * 15));
                bonuss[4].used := 0;
                bar(500, 85 + (4 * 15), 640, 84 + ((4 - 1) * 15));
              end;
          8:  begin
                tr.sticky := 1;
                tr.fire_mode := 0;
                setfillstyle(1,background);
                bonuss[7].used := 0;
                bar(500, 85 + (7 * 15), 640, 84 + ((7 - 1) * 15));
              end;
        end;
        if (bonuss[tile.bon.number_of_bonus].alive = 1) then     {jesli zbilismy bonus, ktory byl juz aktywny, dodajemy 15 sekund do jego trwania}
        begin
          bonuss[tile.bon.number_of_bonus].during_time := bonuss[tile.bon.number_of_bonus].during_time + 15;
        end
        else
        begin
          bonuss[tile.bon.number_of_bonus].alive := 1;          {jesli nie to dodajemy wszystkie jego dane}
          bonuss[tile.bon.number_of_bonus].during_time := 15;
          bonuss[tile.bon.number_of_bonus].time_start := time;
          bonuss[tile.bon.number_of_bonus].used := 1;
          bonuss[tile.bon.number_of_bonus].x := 1;
        end;
        tile.bon.used := 1;           {zmieniamy zurzyty klocek z bonusem, zeby sie nie wyswietla�}
        tile.bon.alive := 0;
        tile.bon.number_of_bonus := 0;
        tile.lifes := 0;
        tile.visible := 1;
      end;
    end;
  end;
end;

{fizyka gry}
function physics(var c : char; var bricks : wall; var missles : missle; var bullet : ball; var tr : tray; var lifes : integer; var points : integer; var bonuss : bonuses) : integer;
var
  i, j, rapidity, temp : integer;
  play : integer;
  win : integer;
begin
  play := 1;
  fire(missles, bricks);  {strzalmy}
  deactivate_bonuses(bricks, bonuss, tr, rapidity);  {zajmujemy sie bonusami}
  tr.move(bullet, c);  {poruszamy tacka}
  if bullet.sticked = 0 then
    bullet.move(bricks, points) {wykonujemy ruch kulki, jesli nie jest przyklejona}
  else
    if c = ' ' then {jesli przyklejona to spacja ja odklei}
    begin
      bullet.sticked := 0;
      bullet.move(bricks, points)
    end;
  temp := lifes;
  tr.bounce(bullet, lifes); {odbiajmy kulke}
  if temp - lifes = 1 then  {jesli w trakcie odbicia sie skulismy}
    play := 0;   {to zwrocimy 0}
  for i := 1 to 8 do    {dla kazdego klocka wykonujemy ewentualne dzialania na bonusach}
    for j:= 1 to 6 do
      destroy(bricks[i][j], tr, lifes, points, rapidity, bonuss);
  if (rapidity = 1) or (rapidity = 2) or (rapidity = 3) then  {jesli zmienila sie predkosc}
    bullet.speed := rapidity;   {to jest nowa}
  if tr.fire_mode = 1 then   {jesli jest opcja strzelania}
    if (c = ' ') and (missles.alive = 0) then   {uwalniamy pocisk}
    begin
      missles := tr.shoot;
      c := 'w';
    end;
  win := 1;
  for i := 1 to 8 do
    for j := 1 to 6 do
      if bricks[i][j].lifes > 0 then win := 0; {s[rawdzamy wygrana}
  if win = 1 then play := 2;
  physics := play;
end;

{komunikaty o bonusach}
procedure communique(i : integer);
begin
  SetTextStyle(0,0,0);
  case i of
    1:  begin
          setcolor(green);
          outtextxy(500,90, 'Zycia +1!');
        end;
    2:  begin
          setcolor(green);
          outtextxy(500,105, 'Punkty +50!');
        end;
    3:  begin
          setcolor(red);
          outtextxy(500,120, 'Waska tacka!');
        end;
    4:  begin
          setcolor(green);
          outtextxy(500,135, 'Szeroka tacka!');
        end;
    5:  begin
          setcolor(red);
          outtextxy(500,150, 'Szybka pilka!');
        end;
    6:  begin
          setcolor(green);
          outtextxy(500,165, 'Wolna pilka!');
        end;
    7:  begin
          setcolor(green);
          outtextxy(500,180, 'Strzaly!');
        end;
    8:  begin
          setcolor(green);
          outtextxy(500,195, 'Lepka tacka!');
        end;
  end;
end;

{wy�wietlanie}
procedure show_all(bricks : wall; var missles : missle; var bullet : ball; var tr : tray; var bonuss : bonuses);
var
  i, j : integer;
begin
  missles.print(blue);
  for i := 1 to 8 do
    for j := 1 to 6 do
      bricks[i][j].put;
  bullet.show(bullet_color);
  tr.draw(yellow);
  for i := 1 to 8 do
    if (bonuss[i].alive = 1) and (bonuss[i].used = 1) and (bonuss[i].x = 1) then
    begin
      communique(i);
      bonuss[i].x := 2;
    end
    else
    if bonuss[i].alive = 0 then
    begin
      setfillstyle(1,background);
      bar(500, 85 + (i * 15), 640, 84 + ((i - 1) * 15));
    end;
end;

{ramka}
procedure frame();
begin
  setcolor(white);
  line(0,0,0,410);
  line(490,0,490,410);
end;

{wyswietl panel boczny}
procedure show(lifes : integer; points : integer);
var
  temp : string;
begin
  SetTextStyle(0,0,0);
  setcolor(white);
  OutTextXY(500, 30, 'GRACZ');
  temp := inttostr(lifes);
  outtextxy(500,45, 'Zycia: ');
  outtextxy(600,45, temp);
  temp := inttostr(points);
  outtextxy(500,60, 'Punkty: ');
  outtextxy(600,60, temp);
  outtextxy(500,75, 'AKTYWNE BONUSY:');
end;

{g��wna p�tla gry}
procedure main_loop(var bricks : wall; var lifes : integer; var points : integer; TIME_STEP : real);
var
  dt, lastUpdateTime, accumulator, MAX_ACCUMULATED_TIME : real;     {czas od ostatniej aktualizacji}
  tr : tray;
  bullet : ball;
  bonuss : bonuses;
  missles : missle;
  play, i, pauses : integer;
  c : char;
  temp : string;
begin
  {tacka}
  tr.x := 210;
  tr.y := 400;
  tr.xp := tr.x;
  tr.yp := tr.y;
  tr.width := 80;
  tr.height := 10;
  tr.fire_mode := 0;
  tr.speed := 3;
  tr.sticky := 0;

  {pociski}
  missles.alive := 0;

  {kulka}
  bullet.x := 250;
  bullet.y := 393;
  bullet.xp := bullet.x;
  bullet.yp := bullet.y;
  bullet.sticked := 0;
  bullet.turn_y := -1;
  bullet.angle := random(30) + 30;
  if bullet.angle mod 2 = 0 then
    bullet.turn_x := -1
  else bullet.turn_x := 1;
  bullet.speed := 2;

  {zerujemy bonusy}
  for i := 1 to 8 do
  begin
    bonuss[i].alive := 0;
    bonuss[i].used := 0;
  end;
  
  pauses := 0;
  play := 1;
  cleardevice;
  dt := 0;
  {wyswietlamy stan poczatkowy}
  lastUpdateTime := get_time; {czas ostatniej aktualizacji}
  show_all(bricks, missles, bullet, tr, bonuss);
  show(lifes, points);
  SetTextStyle(0,0,6);
  setcolor(lightblue);
  outtextxy(120,430,'ARKANOID');
  SetTextStyle(0,0,0);
  frame;
  accumulator := 0.0;
  MAX_ACCUMULATED_TIME := 1.0; {maksymalny czas zgromadzony w pojedynczym obiegu petli glownej        }
  {p�tla czasow}
  while (play = 1) do
  begin
    dt := get_time - lastUpdateTime; {//obliczenie czasu od ostatniej klatki}
    lastUpdateTime := dt + lastUpdateTime;   {podmiana}
    if (dt < 0) then dt := 0;  //upewniamy sie, ze dt >= 0}
    accumulator := accumulator + dt;
    if accumulator > MAX_ACCUMULATED_TIME then
      accumulator := MAX_ACCUMULATED_TIME;
    if accumulator < 0 then
      accumulator := 0; {//zapobiegamy zbyt duzej ilosci aktualizacji w danym obiegu petli glownej}
    if keypressed then c := readkey;  {<-- zbieranie wejscia z klawiatury, myszki, sieci, itp.}
    if c = 'q' then break;     {wyjscie}
    if (c = 'p') and (pauses = 0) then  {pauza}
    begin
      setcolor(white);                  {komentarz}
      outtextxy(500, 10, 'PAUZA');
      pauses := 1;
      c := 'w';
    end
    else
    if (c = 'p') and (pauses = 1) then      {konczymy pauze}
    begin
      setfillstyle(1,black);
      bar(500, 10, 550, 25);
      pauses := 0;
      c := 'w';
    end;
    if (c = ' ') and (bullet.sticked = 0) and (tr.sticky = 1) then c := 'w';
    while (accumulator > TIME_STEP) and (play = 1) do {petla z fizyka}
    begin
      if pauses = 0 then
        play := physics(c, bricks, missles, bullet, tr, lifes, points, bonuss);  {<-- aktualizacja fizyki i logiki gry}
      accumulator := accumulator - TIME_STEP;
    end;
    show_all(bricks, missles, bullet, tr, bonuss);  {wyswietlenie aktualnego stanu na ekranie}
  end;
  if (play = 0) and (lifes = -1) and (c <> 'q') then {przegrana}
  begin
    setcolor(red);
    SetTextStyle(0,0,2);
    outtextxy(140,230, 'KONIEC GRY!');
    delay(500);
    cleardevice;
    setcolor(white);
    SetTextStyle(0,0,1);
    outtextxy(240,230, 'TWOJ WYNIK:');
    temp := inttostr(points);
    outtextxy(340,230, temp);
    outtextxy(180,260, 'Nacisnij q, aby przejsc do menu!');
    repeat
      c := readkey;
    until c = 'q';
  end
  else
  if play = 2 then                    {wygrana}
  begin
    setcolor(green);
    SetTextStyle(0,0,2);
    outtextxy(140,230, 'WYGRALES!');
    delay(800);
    cleardevice;
    setcolor(white);
    SetTextStyle(0,0,1);
    outtextxy(260,230, 'TWOJ WYNIK:');
    points := points + lifes * 200;
    temp := inttostr(points);
    outtextxy(350,230, temp);
    outtextxy(180,260, 'Nacisnij q, aby przejsc do menu!');
    repeat
      c := readkey;
    until c = 'q';
  end
  else
  if (play = 0) and (lifes >= 0) then       {nastepna szansa}
  begin
    setcolor(white);
    outtextxy(190,230, 'NASTEPNA SZANSA!');
    outtextxy(145,250, 'Nacisnij n, aby grac dalej!');
    outtextxy(160,265, 'Nacisnij q, aby wyjsc.');
    repeat
      c := readkey;
    until (c = 'n') or (c = 'q');
    if c = 'n' then
      main_loop(bricks, lifes, points, TIME_STEP);
  end;
end;

{analogicznie dla komputera}
procedure main_loop_computer(var bricks : wall; var lifes : integer; var points : integer);
var
  dt, lastUpdateTime, accumulator, TIME_STEP, MAX_ACCUMULATED_TIME : real;     {czas od ostatniej aktualizacji}
  tr : tray;
  bullet : ball;
  bullet1 : ball;
  bonuss : bonuses;
  missles : missle;
  play, i : integer;
  c, a : char;
  temp : string;
begin
  {tacka}
  tr.x := 210;
  tr.y := 400;
  tr.xp := tr.x;
  tr.yp := tr.y;
  tr.width := 80;
  tr.height := 10;
  tr.fire_mode := 0;
  tr.speed := 3;
  tr.sticky := 0;

  {pociski}
  missles.alive := 0;

  {kulka}
  bullet.x := 250;
  bullet.y := 393;
  bullet.xp := bullet.x;
  bullet.yp := bullet.y;
  bullet.sticked := 0;
  bullet.turn_y := -1;
  bullet.angle := random(30) + 30;
  if bullet.angle mod 2 = 0 then
    bullet.turn_x := -1
  else bullet.turn_x := 1;
  bullet.speed := 2;

  for i := 1 to 8 do
  begin
    bonuss[i].alive := 0;
    bonuss[i].used := 0;
  end;
  
  play := 1;
  cleardevice;
  dt := 0;
  lastUpdateTime := get_time; {czas ostatniej aktualizacji}
  show_all(bricks, missles, bullet, tr, bonuss);
  show(lifes, points);
  SetTextStyle(0,0,6);
  setcolor(lightblue);
  outtextxy(120,430,'ARKANOID');
  SetTextStyle(0,0,0);
  frame;
  accumulator := 0.0;
  TIME_STEP := 0.003;  {krok czasowy, a zarazem czas trwania ramki
fizyki w sekundach; tutaj 30 milisekund, czyli
ok. 30 aktualizacji na sekund�}
  MAX_ACCUMULATED_TIME := 1.0; {maksymalny czas zgromadzony w pojedynczym obiegu petli glownej        }
  while (play = 1) do
  begin
    dt := get_time - lastUpdateTime; {obliczenie czasu od ostatniej klatki}
    lastUpdateTime := dt + lastUpdateTime;   {podmiana}
    if (dt < 0) then dt := 0;  {upewniamy sie, ze dt >= 0}
    accumulator := accumulator + dt;
    if accumulator > MAX_ACCUMULATED_TIME then
      accumulator := MAX_ACCUMULATED_TIME;
    if accumulator < 0 then
      accumulator := 0; {//zapobiegamy zbyt duzej ilosci aktualizacji w danym obiegu petli glownej  }
    bullet1 := bullet;
    if keypressed then a := readkey; {ewentualne konczenie gry}
    if a = 'q' then break;
    repeat   {wykonujemy ruch kulki a� do osiagniecia 410, �eby odczyta� gdzie spadnie}
      move_comp(bullet1, bricks);  {dzialamy na kopii zmiennej bullet}
    until bullet1.y > 400;
    if tr.x + tr.width div 2 - 5 > bullet1.x then c := 'a';            {ruch}
    if tr.x + tr.width div 2 + 5 < bullet1.x then c := 'd';
    if (tr.x + tr.width div 2 + 5 > bullet1.x) and (tr.x + tr.width div 2 - 5 < bullet1.x) then c := 'w';
    if bullet.sticked = 1 then c := ' ';
    while (accumulator > TIME_STEP) and (play = 1) do
    begin
      play := physics(c, bricks, missles, bullet, tr, lifes, points, bonuss);  { aktualizacja fizyki i logiki gry}
      accumulator := accumulator - TIME_STEP;
    end;
    show_all(bricks, missles, bullet, tr, bonuss);  {wyswietlenie aktualnego stanu na ekranie}
  end;
  if (play = 0) and (lifes = -1) and (c <> 'q') then {porazka}
  begin
    setcolor(red);
    SetTextStyle(0,0,2);
    outtextxy(140,230, 'KONIEC GRY!');
    delay(500);
    cleardevice;
    setcolor(white);
    SetTextStyle(0,0,1);
    outtextxy(240,230, 'WYNIK CPU:');
    temp := inttostr(points);
    outtextxy(340,230, temp);
    outtextxy(180,260, 'Nacisnij q, aby przejsc do menu!');
    repeat
      c := readkey;
    until c = 'q';
  end
  else
  if play = 2 then
  begin
    setcolor(green);
    SetTextStyle(0,0,2);
    outtextxy(140,230, 'CPU WYGRAL!');    {wygrana}
    delay(800);
    cleardevice;
    setcolor(white);
    SetTextStyle(0,0,1);
    outtextxy(260,230, 'WYNIK CPU:');
    points := points + lifes * 200;
    temp := inttostr(points);
    outtextxy(340,230, temp);
    outtextxy(180,260, 'Nacisnij q, aby przejsc do menu!');
    repeat
      c := readkey;
    until c = 'q';
  end
  else
  if (play = 0) and (lifes >= 0) then              {kolejna szansa}
  begin
    setcolor(white);
    outtextxy(190,230, 'NASTEPNA SZANSA!');
    delay(600);
    main_loop(bricks, lifes, points, TIME_STEP);
  end;
end;

{funkcja wyboru}
function menu_choose(t : strings; n : integer) : integer;
var
  c, d : char;
  i : integer;
begin
  for i := 1 to n do
  begin
    textcolor(15);
    textbackground(0);
    gotoxy(2, i + 4);
    write(t[i]);
  end;
  i := 1;
  repeat
    textcolor(0);
    textbackground(15);
    gotoxy(2, 4 + i);
    write(t[i]);
    c := readkey;
    d := ' ';
    if c = chr(0) then d := readkey;
    textcolor(0);
    textbackground(15);
    gotoxy(2, 4 + i);
    write(t[i]);
    if d = 'H' then
    begin
      dec(i);
      textcolor(15);
      textbackground(0);
      gotoxy(2, i + 5);
      write(t[i+1]);
    end;
    if d = 'P' then
    begin
      inc(i);
      textcolor(15);
      textbackground(0);
      gotoxy(2, i + 3);
      write(t[i-1]);
    end;
    if i = 0 then i := n;
    if i = n + 1 then i := 1;
  until c = #13;
  textcolor(15);
  textbackground(0);

  menu_choose := i;
end;

{wybor planszy}
procedure choose_board(var bricks : wall; nr : integer);
var
  i, j : integer;
begin
  case nr of
    1: begin
         for i := 1 to 8 do
           for j := 1 to 6 do
           begin
             if ((j = 6) or (((i = 2) or (i = 3) or (i = 4) or (i = 6) or (i = 7) or (i = 8))
             and (j = 5)) or ((j = 4) and ((i = 3) or (i = 7) or (i = 8))) or ((j = 3) and (i = 8))) then
             begin
               bricks[i][j].lifes := 2;
               bricks[i][j].bon.alive := 0;
               bricks[i][j].y := (j - 1) * (bricks_height) + j - 1;
               bricks[i][j].x := (i - 1) * (bricks_width) + i + 1;
               bricks[i][j].bon.number_of_bonus := 0;
               bricks[i][j].color := red;
               bricks[i][j].visible := 1;
               case random(81) mod 9 of
                 0: bricks[i][j].bon.number_of_bonus := 2;
                 1: if random(7) mod 5 = 3 then bricks[i][j].bon.number_of_bonus := 1;
                 2: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 3: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 4: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 5: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 6: bricks[i][j].bon.number_of_bonus := 2;
               end;
             end
             else
             if (i = 5) and (j = 2) then
             begin
               bricks[i][j].lifes := 1;
               bricks[i][j].bon.alive := 0;
               bricks[i][j].y := (j - 1) * (bricks_height) + j - 1;
               bricks[i][j].x := (i - 1) * (bricks_width) + i + 1;
               bricks[i][j].bon.number_of_bonus := 0;
               bricks[i][j].color := red;
               bricks[i][j].visible := 1;
               case random(81) mod 9 of
                 0: bricks[i][j].bon.number_of_bonus := 2;
                 1: if random(7) mod 5 = 3 then bricks[i][j].bon.number_of_bonus := 1;
                 2: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 3: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 4: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 5: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 6: bricks[i][j].bon.number_of_bonus := 2;
               end;
             end
             else
             if ((j = 1) and ((i = 1) or (i = 2) or (i = 3) or (i = 7) or (i = 8))) or ((j = 2) and ((i = 1) or (i = 2))) then
             begin
               bricks[i][j].lifes := 3;
               bricks[i][j].bon.alive := 0;
               bricks[i][j].y := (j - 1) * (bricks_height) + j - 1;
               bricks[i][j].x := (i - 1) * (bricks_width) + i + 1;
               bricks[i][j].bon.number_of_bonus := 0;
               bricks[i][j].color := red;
               bricks[i][j].visible := 1;
               case random(81) mod 9 of
                 0: bricks[i][j].bon.number_of_bonus := 2;
                 1: if random(7) mod 5 = 3 then bricks[i][j].bon.number_of_bonus := 1;
                 2: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 3: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 4: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 5: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 6: bricks[i][j].bon.number_of_bonus := 2;
               end;
             end
             else
             begin
               bricks[i][j].lifes := 0;
               bricks[i][j].bon.alive := 0;
               bricks[i][j].y := (j - 1) * (bricks_height) + j - 1;
               bricks[i][j].x := (i - 1) * (bricks_width) + i + 1;
               bricks[i][j].bon.number_of_bonus := 0;
               bricks[i][j].color := red;
               bricks[i][j].visible := 1;
             end
           end;
    end;
    2:  begin
         for i := 1 to 8 do
           for j := 1 to 6 do
           begin
             if (j = 1) or (j = 6) then
             begin
               bricks[i][j].lifes := 1;
               bricks[i][j].bon.alive := 0;
               bricks[i][j].y := (j - 1) * (bricks_height) + j - 1;
               bricks[i][j].x := (i - 1) * (bricks_width) + i + 1;
               bricks[i][j].bon.number_of_bonus := 0;
               bricks[i][j].color := red;
               bricks[i][j].visible := 1;
               case random(81) mod 9 of
                 0: bricks[i][j].bon.number_of_bonus := 2;
                 1: if random(7) mod 5 = 3 then bricks[i][j].bon.number_of_bonus := 1;
                 2: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 3: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 4: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 5: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 6: bricks[i][j].bon.number_of_bonus := 2;
               end;
             end
             else
             if ((j = 5) or (j = 2)) and ((i <>1) and (i <> 8)) then
             begin
               bricks[i][j].lifes := 2;
               bricks[i][j].bon.alive := 0;
               bricks[i][j].y := (j - 1) * (bricks_height) + j - 1;
               bricks[i][j].x := (i - 1) * (bricks_width) + i + 1;
               bricks[i][j].bon.number_of_bonus := 0;
               bricks[i][j].color := red;
               bricks[i][j].visible := 1;
               case random(81) mod 9 of
                 0: bricks[i][j].bon.number_of_bonus := 2;
                 1: if random(7) mod 5 = 3 then bricks[i][j].bon.number_of_bonus := 1;
                 2: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 3: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 4: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 5: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 6: bricks[i][j].bon.number_of_bonus := 2;
               end;
             end
             else
             if ((j = 3) or (j = 4)) and ((i <> 1) and (i <> 8) and (i <> 7) and (i <> 2)) then
             begin
               bricks[i][j].lifes := 3;
               bricks[i][j].bon.alive := 0;
               bricks[i][j].y := (j - 1) * (bricks_height) + j - 1;
               bricks[i][j].x := (i - 1) * (bricks_width) + i + 1;
               bricks[i][j].bon.number_of_bonus := 0;
               bricks[i][j].color := red;
               bricks[i][j].visible := 1;
               case random(81) mod 9 of
                 0: bricks[i][j].bon.number_of_bonus := 2;
                 1: if random(7) mod 5 = 3 then bricks[i][j].bon.number_of_bonus := 1;
                 2: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 3: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 4: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 5: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 6: bricks[i][j].bon.number_of_bonus := 2;
               end;
             end
             else
             begin
               bricks[i][j].lifes := 0;
               bricks[i][j].bon.alive := 0;
               bricks[i][j].y := (j - 1) * (bricks_height) + j - 1;
               bricks[i][j].x := (i - 1) * (bricks_width) + i + 1;
               bricks[i][j].bon.number_of_bonus := 0;
               bricks[i][j].color := red;
               bricks[i][j].visible := 1;
             end
        end;
      end;
    3: begin
         for i := 1 to 8 do
           for j := 1 to 6 do
           begin
             if ((j = 1) and ((i = 2) or (i = 7))) or ((j = 2) and ((i = 1) or (i = 3) or (i = 6) or (i = 8)))
             or ((j = 3) and ((i = 2) or (i = 4) or (i = 5) or (i = 7))) or ((j = 4) and ((i = 3) or (i = 6)))
             or ((j = 5) and ((i = 4) or (i = 5))) then
             begin
               bricks[i][j].lifes := 1;
               bricks[i][j].bon.alive := 0;
               bricks[i][j].y := (j - 1) * (bricks_height) + j - 1;
               bricks[i][j].x := (i - 1) * (bricks_width) + i + 1;
               bricks[i][j].bon.number_of_bonus := 0;
               bricks[i][j].color := red;
               bricks[i][j].visible := 1;
               case random(81) mod 9 of
                 0: bricks[i][j].bon.number_of_bonus := 2;
                 1: if random(7) mod 5 = 3 then bricks[i][j].bon.number_of_bonus := 1;
                 2: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 3: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 4: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 5: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 6: bricks[i][j].bon.number_of_bonus := 2;
               end;
             end
             else
             if ((j = 1) and ((i = 1) or (i = 8))) or ((j = 2) and ((i = 2) or (i = 7)))
             or ((j = 3) and ((i = 1) or (i = 3) or (i = 6) or (i = 8))) or ((j = 4) and ((i = 2) or (i = 4) or (i = 5) or (i = 7)))
             or ((j = 5) and ((i = 3) or (i = 6))) or ((j = 6) and ((i = 4) or (i = 5)))   then
             begin
               bricks[i][j].lifes := 2;
               bricks[i][j].bon.alive := 0;
               bricks[i][j].y := (j - 1) * (bricks_height) + j - 1;
               bricks[i][j].x := (i - 1) * (bricks_width) + i + 1;
               bricks[i][j].bon.number_of_bonus := 0;
               bricks[i][j].color := red;
               bricks[i][j].visible := 1;
               case random(81) mod 9 of
                 0: bricks[i][j].bon.number_of_bonus := 2;
                 1: if random(7) mod 5 = 3 then bricks[i][j].bon.number_of_bonus := 1;
                 2: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 3: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 4: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 5: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 6: bricks[i][j].bon.number_of_bonus := 2;
               end;
             end
             else
             if ((j = 1) and (i <> 1) and (i <> 8) and (i <> 7) and (i <> 2))
             or ((j = 2) and ((i = 4) or (i = 5))) then
             begin
               bricks[i][j].lifes := 3;
               bricks[i][j].bon.alive := 0;
               bricks[i][j].y := (j - 1) * (bricks_height) + j - 1;
               bricks[i][j].x := (i - 1) * (bricks_width) + i + 1;
               bricks[i][j].bon.number_of_bonus := 0;
               bricks[i][j].color := red;
               bricks[i][j].visible := 1;
               case random(81) mod 9 of
                 0: bricks[i][j].bon.number_of_bonus := 2;
                 1: if random(7) mod 5 = 3 then bricks[i][j].bon.number_of_bonus := 1;
                 2: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 3: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 4: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 5: bricks[i][j].bon.number_of_bonus := random(7) + 2;
                 6: bricks[i][j].bon.number_of_bonus := 2;
               end;
             end
             else
             begin
               bricks[i][j].lifes := 0;
               bricks[i][j].bon.alive := 0;
               bricks[i][j].y := (j - 1) * (bricks_height) + j - 1;
               bricks[i][j].x := (i - 1) * (bricks_width) + i + 1;
               bricks[i][j].bon.number_of_bonus := 0;
               bricks[i][j].color := red;
               bricks[i][j].visible := 1;
             end
        end;
      end;
    4: begin
         for i := 1 to 8 do
           for j := 1 to 6 do
           begin
             bricks[i][j].lifes := random(4) mod 3 + 1;
             bricks[i][j].bon.alive := 0;
             bricks[i][j].y := (j - 1) * (bricks_height) + j - 1;
             bricks[i][j].x := (i - 1) * (bricks_width) + i + 1;
             bricks[i][j].bon.number_of_bonus := 0;
             bricks[i][j].color := red;
             bricks[i][j].visible := 1;
             case random(81) mod 9 of
               0: bricks[i][j].bon.number_of_bonus := 2;
               1: if random(7) mod 5 = 3 then bricks[i][j].bon.number_of_bonus := 1;
               2: bricks[i][j].bon.number_of_bonus := random(7) + 2;
               3: bricks[i][j].bon.number_of_bonus := random(7) + 2;
               4: bricks[i][j].bon.number_of_bonus := random(7) + 2;
               5: bricks[i][j].bon.number_of_bonus := random(7) + 2;
               6: bricks[i][j].bon.number_of_bonus := 2;
             end;
           end;
       end;
   end;
end;

{g�owna funkcja}
procedure menu(var board : integer; var ts : real);
var
  choose, choose1, choose2 : strings;
  c : char;
  best_scores : results;
  best : text;
  lifes, points, i, j : integer;
  bricks : wall;
  temp : integer;
begin
  assign(best, 'E:/GitHub/WDI-IS-2012/projekt/najlepsze.txt');
  {$I-}     {otwieramy albo tworzymy plik}
  reset(best);
  if ioresult <> 0 then rewrite(best);
  close(best);
  {$I+}
  clrscr;
  textcolor(yellow);
  textbackground(0);
  gotoxy(5, 3);
  write('ARKANOID - MENU');
  textcolor(white);
  choose[1] := 'Nowa gra';
  choose[2] := 'Wybor poziomu';
  choose[3] := 'Wybor planszy';
  choose[4] := 'Instrukcja';
  choose[5] := 'Gracz CPU';
  choose[6] := 'Najlepsze wyniki';
  choose[7] := 'Autor';
  choose[8] := 'Zakoncz';
  case menu_choose(choose, 8) of
    1: begin     {nowa gra, zapis wyniku do pliku}
         graphic;
         lifes := 2;
         points := 0;
         choose_board(bricks, board);
         main_loop(bricks, lifes, points, ts);
         CloseGraph;
         reset(best);
         i := 1;
         j := 1;
         while not eof(best) do
         begin
           readln(best, temp);
           if points > temp then
           begin
             best_scores[i] := points;
             if (j = 1) then
             begin
               clrscr;
               writeln;
               writeln('To ', i,'. wynik!');
               delay(1200);
             end;
             j := j + 1;
             points := temp;
           end
           else
             best_scores[i] := temp;
           i := i + 1;
         end;
         if i < 6 then best_scores[i] := points;
         close(best);
         rewrite(best);
         for i := 1 to 5 do
           writeln(best, best_scores[i]);
         close(best);
         menu(board, ts);
       end;


    2: begin           {wybor poziomu}
         clrscr;
         choose2[1] := 'Latwy';
         choose2[2] := 'Normalny';
         choose2[3] := 'Trudny';
         choose2[4] := 'Ekstremalny';
         textcolor(yellow);
         textbackground(0);
         gotoxy(5, 3);
         write('ARKANOID - WYBOR POZIOMU');
         textcolor(white);
         case menu_choose(choose2, 4) of
           1: ts := 0.005;
           2: ts := 0.004;
           3: ts := 0.003;
           4: ts := 0.002;
         end;
         menu(board, ts);
       end;


    3: begin           {wybor planszy}
         clrscr;
         choose1[1] := 'Piramidy';
         choose1[2] := 'Kolorki';
         choose1[3] := 'Schody';
         choose1[4] := 'Domyslna';
         textcolor(yellow);
         textbackground(0);
         gotoxy(5, 3);
         write('ARKANOID - WYBOR PLANSZY');
         textcolor(white);
         board := menu_choose(choose1, 4);
         menu(board, ts);
       end;


    4: begin           {instrukcja}
         clrscr;
         textcolor(yellow);
         textbackground(0);
         gotoxy(5, 3);
         writeln('ARKANOID - INSTRUKCJA');
         textcolor(white);
         writeln;
         writeln(' Celem gracza jest zdobycie jak najwiekszej liczby punktow ');
         writeln(' w grze. Gracz otrzymuje 10 punktow za kazde zbicie, ');
         writeln(' 200 za kazde zachowane zycie na koncu gry oraz ');
         writeln(' 50 w przypadku zbicia odpowiedniego bonusa. ');
         writeln(' Za zestrzelone cegielki gracz nie otrzymuje punktow.');
         writeln;
         textcolor(yellow);
         writeln('    STEROWANIE');
         textcolor(white);
         writeln(' a/d - poruszanie tacka (lewo/prawo)');
         writeln(' spacja - strzal/odklejenie pileczki (w przypadku bonusow)');
         writeln(' p - pausa');
         writeln(' q - wyjscie');
         writeln(' dowolny (oprocz a,d,p,q) - zatrzymanie tacki');
         writeln;
         writeln;
         writeln(' Nacinij Esc, aby wrocic do menu');
         repeat
           c := readkey;
         until c = #27;
         menu(board, ts);
       end;


    5: begin               {gra komputera}
         graphic;
         lifes := 2;
         points := 0;
         choose_board(bricks, board);
         main_loop_computer(bricks, lifes, points);
         CloseGraph;
         menu(board, ts);
       end;


    6: begin          {najlepsze wyniki}
         clrscr;
         textcolor(yellow);
         textbackground(0);
         gotoxy(5, 3);
         write('ARKANOID - NAJLEPSZE WYNIKI');
         textcolor(white);
         reset(best);
         i := 1;
         writeln;
         writeln;
         while not eof(best) do
         begin
           readln(best, temp);
           writeln(' ', i, '. ', temp);
           i := i + 1;
         end;
         close(best);
         writeln;
         writeln(' Nacisnij Esc, aby wrocic do menu.');
         repeat
           c := readkey;
         until c = #27;
         menu(board, ts);
       end;


    7: begin          {autor}
         clrscr;
         textcolor(yellow);
         textbackground(0);
         gotoxy(5, 3);
         write('ARKANOID - AUTOR');
         textcolor(white);
         writeln;
         writeln;
         writeln(' Program na potrzeby WDI stworzyl Wojciech Kasperek.');
         writeln;
         writeln(' Nacinij Esc, aby wrocic do menu');
         repeat
           c := readkey;
         until c = #27;
         menu(board, ts);
       end;


    8: halt;
  end;
end;


begin
  board := 4;
  ts := 0.004; {time step}
  randomize;
  menu(board, ts);
end.
