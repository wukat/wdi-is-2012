unit complex;

interface
uses fractions;
type
  complex_number = record
    re : fraction;
    im : fraction;
  end;

operator +(a,b : complex_number) r : complex_number;
operator -(a,b : complex_number) r : complex_number;
operator *(a,b : complex_number) r : complex_number;
procedure show_complex(a : complex_number; SoN : char);


implementation

operator +(a,b : complex_number) r : complex_number;
begin
  r.re := a.re + b.re;
  r.im := a.im + b.im;
end;

operator -(a,b : complex_number) r : complex_number;
begin
  r.re := a.re - b.re;
  r.im := a.im - b.im;
end;


operator *(a,b : complex_number) r : complex_number;
begin
  r.re := (a.re * b.re) - (a.im * b.im);
  r.im := (a.re * b.im) + (b.re * a.im);
end;

procedure show_complex(a : complex_number; SoN : char);
begin
  if ((SoN = 'c') and (a.im.co > 0)) then
  begin
    show_fr(a.re);
    write(' + ');
    show_fr(a.im);
    write('i ');
  end
  else
    if ((SoN = 'c') and (a.im.co < 0)) then
    begin
      show_fr(a.re);
      write(' - ');
      a.im.co := -a.im.co;
      show_fr(a.im);
      write('i ');
    end;
end;

begin
end.

